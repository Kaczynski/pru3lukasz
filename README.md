#Practica Unidad 3 
A simple career management application using Hibernate.
## Getting Started
### Windows
To clone the repository in a Windows system open the Git console and copy the 
following instruction in the console:
```
git clone https://Kaczynski@bitbucket.org/Kaczynski/pru3lukasz.git
```
### Linux
To clone the repository, copy the following instruction in your console:
```
git clone https://Kaczynski@bitbucket.org/Kaczynski/pru3lukasz.git
```
### Prerequisites

* Java 1.8.
* MariaDB Server 10.1.37. or/and PostgreSQL 11.1

## Run the application

### From console:

```
java -jar pru3lukasz.jar
```

##Author
Lukasz Kaczynski
