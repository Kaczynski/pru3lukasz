package pru3.lukasz.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "escuderia", catalog = "pru3lukasz")
public class Escuderia {
    private long id;
    private String nombre;
    private double presupuesto;
    private Date fechaApertura;
    private List<Coche> coches;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = false, length = 255)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "presupuesto", nullable = false, precision = 0)
    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    @Basic
    @Column(name = "fecha_apertura", nullable = false)
    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Escuderia escuderia = (Escuderia) o;
        return id == escuderia.id &&
                Double.compare(escuderia.presupuesto, presupuesto) == 0 &&
                Objects.equals(nombre, escuderia.nombre) &&
                Objects.equals(fechaApertura, escuderia.fechaApertura);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, presupuesto, fechaApertura);
    }

    @OneToMany(mappedBy = "escuderia")
    public List<Coche> getCoches() {
        return coches;
    }

    public void setCoches(List<Coche> coches) {
        this.coches = coches;
    }

    @Override
    public String toString() {
        return id+" - "+nombre;
    }
}
