package pru3.lukasz.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "patrocinador", catalog = "pru3lukasz")
public class Patrocinador {
    private long id;
    private String nombre;
    private double presupuesto;
    private Date fechaContrato;
    private Coche coche;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = false, length = 255)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "presupuesto", nullable = false, precision = 0)
    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    @Basic
    @Column(name = "fecha_contrato", nullable = false)
    public Date getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(Date fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patrocinador that = (Patrocinador) o;
        return id == that.id &&
                Double.compare(that.presupuesto, presupuesto) == 0 &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(fechaContrato, that.fechaContrato);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, presupuesto, fechaContrato);
    }

    @ManyToOne
    @JoinColumn(name = "id_coche", referencedColumnName = "id")
    public Coche getCoche() {
        return coche;
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }

    @Override
    public String toString() {
        return id+" - "+nombre;
    }
}
