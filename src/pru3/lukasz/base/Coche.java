package pru3.lukasz.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "coche", catalog = "pru3lukasz")
public class Coche {
    private long id;
    private int numero;
    private String color;
    private byte estrellado;
    private Escuderia escuderia;
    private List<Patrocinador> patrocinadores;
    private List<Piloto> pilotos;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "numero", nullable = false)
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "color", nullable = false, length = 255)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "estrellado", nullable = false)
    public byte getEstrellado() {
        return estrellado;
    }

    public void setEstrellado(byte estrellado) {
        this.estrellado = estrellado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coche coche = (Coche) o;
        return id == coche.id &&
                numero == coche.numero &&
                estrellado == coche.estrellado &&
                Objects.equals(color, coche.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numero, color, estrellado);
    }

    @ManyToOne
    @JoinColumn(name = "id_escuderia", referencedColumnName = "id")
    public Escuderia getEscuderia() {
        return escuderia;
    }

    public void setEscuderia(Escuderia escuderia) {
        this.escuderia = escuderia;
    }

    @OneToMany(mappedBy = "coche")
    public List<Patrocinador> getPatrocinadores() {
        return patrocinadores;
    }

    public void setPatrocinadores(List<Patrocinador> patrocinadores) {
        this.patrocinadores = patrocinadores;
    }

    @OneToMany(mappedBy = "coche")
    public List<Piloto> getPilotos() {
        return pilotos;
    }

    public void setPilotos(List<Piloto> pilotos) {
        this.pilotos = pilotos;
    }

    @Override
    public String toString() {
        return id+" - Numero: "+numero;
    }
}
