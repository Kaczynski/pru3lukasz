package pru3.lukasz.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "carrera", catalog = "pru3lukasz")
public class Carrera {
    private long id;
    private double premio;
    private int vueltas;
    private Date fecha;
    private Circuito circuito;
    private List<Piloto> pilotos;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "premio", nullable = false, precision = 0)
    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

    @Basic
    @Column(name = "vueltas", nullable = false)
    public int getVueltas() {
        return vueltas;
    }

    public void setVueltas(int vueltas) {
        this.vueltas = vueltas;
    }

    @Basic
    @Column(name = "fecha", nullable = false)
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carrera carrera = (Carrera) o;
        return id == carrera.id &&
                Double.compare(carrera.premio, premio) == 0 &&
                vueltas == carrera.vueltas &&
                Objects.equals(fecha, carrera.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, premio, vueltas, fecha);
    }

    @ManyToOne
    @JoinColumn(name = "id_circuito", referencedColumnName = "id")
    public Circuito getCircuito() {
        return circuito;
    }

    public void setCircuito(Circuito circuito) {
        this.circuito = circuito;
    }

    @ManyToMany
    @JoinTable(name = "participante", catalog = "pru3lukasz", joinColumns = @JoinColumn(name = "id_carrera", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_piloto", referencedColumnName = "id", nullable = false))
    public List<Piloto> getPilotos() {
        return pilotos;
    }

    public void setPilotos(List<Piloto> pilotos) {
        this.pilotos = pilotos;
    }

    @Override
    public String toString() {
        return id+" - "+fecha;
    }
}
