package pru3.lukasz.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "circuito", catalog = "pru3lukasz")
public class Circuito {
    private long id;
    private String nombre;
    private double longitud;
    private Date fechaEstreno;
    private List<Carrera> carreras;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = false, length = 255)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "longitud", nullable = false, precision = 0)
    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Basic
    @Column(name = "fechaEstreno", nullable = false)
    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circuito circuito = (Circuito) o;
        return id == circuito.id &&
                Double.compare(circuito.longitud, longitud) == 0 &&
                Objects.equals(nombre, circuito.nombre) &&
                Objects.equals(fechaEstreno, circuito.fechaEstreno);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, longitud, fechaEstreno);
    }

    @OneToMany(mappedBy = "circuito")
    public List<Carrera> getCarreras() {
        return carreras;
    }

    public void setCarreras(List<Carrera> carreras) {
        this.carreras = carreras;
    }

    @Override
    public String toString() {
        return id+" - "+nombre;
    }
}
