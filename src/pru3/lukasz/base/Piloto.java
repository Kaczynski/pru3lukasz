package pru3.lukasz.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "piloto", catalog = "pru3lukasz")
public class Piloto {
    private long id;
    private String nombre;
    private double salario;
    private Date fechaNacimiento;
    private List<Carrera> carreras;
    private Coche coche;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = false, length = 255)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "salario", nullable = false, precision = 0)
    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    @Basic
    @Column(name = "fecha_nacimiento", nullable = false)
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piloto piloto = (Piloto) o;
        return id == piloto.id &&
                Double.compare(piloto.salario, salario) == 0 &&
                Objects.equals(nombre, piloto.nombre) &&
                Objects.equals(fechaNacimiento, piloto.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, salario, fechaNacimiento);
    }

    @ManyToMany(mappedBy = "pilotos")
    public List<Carrera> getCarreras() {
        return carreras;
    }

    public void setCarreras(List<Carrera> carreras) {
        this.carreras = carreras;
    }

    @ManyToOne
    @JoinColumn(name = "id_coche", referencedColumnName = "id")
    public Coche getCoche() {
        return coche;
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }

    @Override
    public String toString() {
        return id+" - "+nombre;
    }
}
