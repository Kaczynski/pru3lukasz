package pru3.lukasz;

import pru3.lukasz.login.Registro;
import pru3.lukasz.mvc.Controller;
import pru3.lukasz.mvc.Gui;
import pru3.lukasz.mvc.Model;
import pru3.lukasz.utils.Config;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {


        if(Registro.getInstance().authUser()) {
            try {
                Config.loadProperties();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            System.out.println("iniciando modelo...");
            Model model = new Model();
            System.out.println("iniciando gui...");
            Gui gui = new Gui();
            System.out.println("iniciando controlador...");
            new Controller(model, gui);
        }
    }
}
